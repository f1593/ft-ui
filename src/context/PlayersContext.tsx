import React, { useEffect, useState } from "react";
import axios from "axios";
import { useTranslation } from "react-i18next";
import { message } from "antd";

import { Player, PlayerType } from "../@types";
import config from "../services/config";
import Loader from "../components/Loader";

interface PlayersContextType {
  players: Player[];
  refreshPlayers: () => void;
  addPlayer: (name: string, type: PlayerType) => Promise<void>;
}

const PlayersContext = React.createContext<PlayersContextType>(null);

const PlayersProvider: React.FC = ({ children }) => {
  const [players, setPlayers] = useState<Player[]>(null);
  const [loading, setLoading] = useState(true);
  const { t } = useTranslation();

  const refreshPlayers = () => {
    setLoading(true);
    axios.get(`${config.apiBaseHost}/player`)
      .then((p) => {
        setPlayers(p.data);
        setLoading(false);
      })
      .catch(() => message.error(t("message.error")));
  };
  useEffect(() => refreshPlayers(), []);

  const addPlayer = (name: string, type: PlayerType) => axios.post(`${config.apiBaseHost}/player`, { name, type })
    .then((res) => setPlayers([...players, res.data]))
    .catch(() => message.error(t("message.error")));

  if (loading) return <Loader text={t("appLoading")} />;

  return (
    <PlayersContext.Provider
      value={{
        players,
        refreshPlayers,
        addPlayer,
      }}
    >
      {children}
    </PlayersContext.Provider>
  );
};

export default PlayersContext;
export { PlayersProvider };
