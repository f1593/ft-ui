import React, { useEffect, useState } from "react";
import {
  Layout, Menu, Row,
} from "antd";
import {
  DashboardOutlined, PlayCircleOutlined, PlusCircleOutlined, TeamOutlined,
} from "@ant-design/icons";
import { Link, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Header } from "antd/lib/layout/layout";

const { Sider, Content } = Layout;

const AppLayout: React.FC = ({ children }) => {
  const { pathname } = useLocation();
  const { t } = useTranslation();
  const [keyOpen, setKeyOpen] = useState<string>();
  const currentGame = localStorage.getItem("current-game");

  useEffect(() => setKeyOpen(pathname.split("/")[1]),
    [pathname]);

  return (
    <>
      <Layout>
        <Sider
          breakpoint="md"
          collapsedWidth="0"
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" selectedKeys={[keyOpen]}>
            <Menu.Item key="" icon={<DashboardOutlined />}>
              <Link to="/">{t("dashboard")}</Link>
            </Menu.Item>
            <Menu.Item key="teams-players" icon={<TeamOutlined />}>
              <Link to="/teams-players">{t("teamsPlayers")}</Link>
            </Menu.Item>
            <Menu.Item key="new-game" icon={<PlusCircleOutlined />}>
              <Link to="/new-game">{t("newGame")}</Link>
            </Menu.Item>
            {currentGame && (
              <Menu.Item key="current-game" icon={<PlayCircleOutlined />} style={{ backgroundColor: "#af0303" }}>
                <Link to="/current-game">{t("currentGame")}</Link>
              </Menu.Item>
            )}
          </Menu>
        </Sider>
        <Layout>
          <Header className="site-layout-sub-header-background" style={{ padding: 0 }}>
            <Row justify="space-between" />
          </Header>
          <Content>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
              {children}
            </div>
          </Content>
          {/* <Footer style={{ textAlign: "center" }}>Ant Design ©2018 Created by Ant UED</Footer> */}
        </Layout>
      </Layout>
    </>
  );
};

export default AppLayout;
