import { LoadingOutlined } from "@ant-design/icons";
import React from "react";

import logo from "../logo-black.png";

interface Props {
  text?: string;
}

const Loader: React.FC<Props> = ({ text }) => (
  <>
    <div style={{ textAlign: "center" }}>
      <img src={logo} alt="logo" style={{ width: 500 }} />
    </div>
    <div style={{ textAlign: "center" }}>
      <LoadingOutlined style={{ fontSize: 72, color: "#1890ff" }} />
    </div>
    {text && (
      <div style={{ textAlign: "center", marginTop: 15 }}>
        <p>{text}</p>
      </div>
    )}
  </>
);

export default Loader;
