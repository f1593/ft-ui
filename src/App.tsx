import React from "react";
import {
  BrowserRouter as Router, Switch, Route,
} from "react-router-dom";

import AppLayout from "./components/AppLayout";
import CurrentGame from "./views/CurrentGame/CurrentGame";
import Dashboard from "./views/Dashboard";
import NewGame from "./views/NewGame/NewGame";
import TeamsPlayers from "./views/TeamsPlayers";

function App() {
  return (
    <div className="App">
      <Router>
        <AppLayout>
          <Switch>
            <Route path="/teams-players/:uuid" component={TeamsPlayers} />
            <Route path="/teams-players" component={TeamsPlayers} />
            <Route path="/new-game" component={NewGame} />
            <Route path="/current-game" component={CurrentGame} />
            <Route path="/" component={Dashboard} />
          </Switch>
        </AppLayout>
      </Router>

    </div>
  );
}

export default App;
