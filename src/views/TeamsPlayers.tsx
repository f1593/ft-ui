import { CloseCircleOutlined } from "@ant-design/icons";
import {
  AutoComplete, Button, Card, message, Popconfirm, Table,
} from "antd";
import Title from "antd/lib/typography/Title";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router-dom";

import { Game, Player } from "../@types";
import PlayersContext from "../context/PlayersContext";
import config from "../services/config";

interface GameResume {
  key: string;
  date: string;
  status: string;
  player1Name: string;
  player2Name: string;
  score1: number;
  score2: number;
}

const TeamPlayers = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const params = useParams<{uuid: string}>();
  const { players, refreshPlayers } = useContext(PlayersContext);

  const [value, setValue] = useState<string>("");
  const [options, setOptions] = useState<{ ref: string, value: string }[]>([]);
  const [player, setPlayer] = useState<Player>();
  const [games, setGames] = useState<GameResume[]>([]);
  const [loading, setLoading] = useState(true);

  const fetchGames = () => {
    setLoading(true);
    axios.get<Game[]>(`${config.apiBaseHost}/game?player=${params.uuid}`)
      .then((res) => {
        setGames(res.data
          .map((g) => {
            const {
              date, score1, score2, status,
            } = g;
            const p1 = players.find((p) => p.uuid === g.player1);
            const p2 = players.find((p) => p.uuid === g.player2);
            return {
              key: g.uuid,
              date,
              status,
              player1Name: p1?.name,
              player2Name: p2?.name,
              score1,
              score2,
            };
          })
          .sort((a, b) => (new Date(a.date).getTime()) + (new Date(b.date).getTime())));
        setLoading(false);
      })
      .catch(() => message.error(t("message.error")));
  };

  useEffect(() => {
    if (params.uuid) {
      setPlayer(players.find((p) => p.uuid === params.uuid));
      fetchGames();
    }
  }, [params]);

  const onSearch = (search: string) => {
    setOptions(
      players
        .filter((p) => p.name.toLowerCase().includes(search.toLowerCase()))
        .map((p) => ({ ref: p.uuid, value: p.name })),
    );
  };

  const onChange = (val: string) => setValue(val);

  const onSelect = (val: string, label: any) => history.push(`/teams-players/${label.ref}`);

  const onCancel = (gameUUID: string) => () => {
    axios.put(`${config.apiBaseHost}/game/${gameUUID}/action`, { action: "GAME_CANCELED" })
      .then(() => {
        fetchGames();
        refreshPlayers();
      })
      .catch(() => message.error(t("message.error")));
  };

  const renderCancelButton = (gameUUID: string) => (
    <Popconfirm
      title={t("message.confirmCancel")}
      okText="Yes"
      cancelText="No"
      onConfirm={onCancel(gameUUID)}
    >
      <Button danger><CloseCircleOutlined /></Button>
    </Popconfirm>
  );

  const columns = [{
    title: t("stats.date"),
    dataIndex: "date",
    key: "date",
  }, {
    title: t("stats.status"),
    dataIndex: "status",
    key: "status",
    render: (text) => t(`status.${text}`),
  }, {
    title: t("stats.player1"),
    dataIndex: "player1Name",
    key: "player1Name",
    render: (text, record) => <p className={record.score1 > record.score2 ? "green" : "red"}>{text}</p>,
  }, {
    title: t("stats.player2"),
    dataIndex: "player2Name",
    key: "player2Name",
    render: (text, record) => <p className={record.score2 > record.score1 ? "green" : "red"}>{text}</p>,
  }, {
    title: t("stats.scorePlayer1"),
    dataIndex: "score1",
    key: "score1",
    render: (text, record) => <p className={record.score1 > record.score2 ? "green" : "red"}>{text}</p>,
  }, {
    title: t("stats.scorePlayer2"),
    dataIndex: "score2",
    key: "score2",
    render: (text, record) => <p className={record.score2 > record.score1 ? "green" : "red"}>{text}</p>,
  }, {
    title: t("Actions"),
    render: (_, record) => (record.status !== "CANCELED" ? renderCancelButton(record.key) : <></>),
  }];

  return (
    <>
      <Title>{t("teamsPlayers")}</Title>
      <AutoComplete
        style={{ width: 500 }}
        options={options}
        placeholder={t("searchTeamPlayer")}
        onSearch={onSearch}
        onChange={onChange}
        onSelect={onSelect}
        value={value}
      />
      {player && (
        <Card style={{ marginTop: 10 }}>
          <Title>{player.name}</Title>
          <Table columns={columns} dataSource={games} loading={loading} />
        </Card>
      )}
    </>
  );
};

export default TeamPlayers;
