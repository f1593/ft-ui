import { MinusCircleOutlined, PlusCircleOutlined } from "@ant-design/icons";
import {
  Button, Card, Col, message, Row,
} from "antd";
import Title from "antd/lib/typography/Title";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

import { Game } from "../../@types";
import PlayersContext from "../../context/PlayersContext";
import config from "../../services/config";

const CurrentGame = () => {
  const { t } = useTranslation();
  const { players, refreshPlayers } = useContext(PlayersContext);
  const currentGameUUID = localStorage.getItem("current-game");
  if (!currentGameUUID) {
    useHistory().push("/");
    return <></>;
  }
  const findPlayerName = (uuid) => players.find((p) => p.uuid === uuid).name;
  const [currentGame, setCurrentGame] = useState<Game>();

  useEffect(() => {
    axios.get<Game>(`${config.apiBaseHost}/game/${currentGameUUID}`)
      .then((res) => setCurrentGame(res.data))
      .catch(() => message.error(t("message.error")));
  }, []);

  useEffect(() => {
    if (currentGame && ["FINISHED", "CANCELED"].includes(currentGame.status)) {
      localStorage.removeItem("current-game");
      message.success(t("message.gameFinished"));
      refreshPlayers();
    }
  }, [currentGame]);

  const sendMatchAction = (action: string) => axios.put<Game>(`${config.apiBaseHost}/game/${currentGameUUID}/action`, { action })
    .then((res) => setCurrentGame(res.data))
    .catch(() => message.error(t("message.error")));

  return (
    <>
      <Title>{t("currentGame")}</Title>
      {currentGame && (
        <>
          <Row justify="center" style={{ marginTop: 40 }}>
            <Col span={8}>
              <Card>
                <Row justify="center" style={{ fontSize: 20 }}>
                  {findPlayerName(currentGame.player1)}
                </Row>
                <Row justify="center" style={{ fontSize: 70, marginBottom: 20 }}>
                  {currentGame.score1}
                </Row>
                <Row justify="center" style={{ fontSize: 20 }}>
                  <Button type="text" size="large" icon={<MinusCircleOutlined />} onClick={() => sendMatchAction("P1_UNSCORED")} />
                  <Button type="text" size="large" icon={<PlusCircleOutlined />} onClick={() => sendMatchAction("P1_SCORED")} />
                </Row>
              </Card>
            </Col>
            <Col offset={2} span={8}>
              <Card>
                <Row justify="center" style={{ fontSize: 20 }}>
                  {findPlayerName(currentGame.player2)}
                </Row>
                <Row justify="center" style={{ fontSize: 70, marginBottom: 20 }}>
                  {currentGame.score2}
                </Row>
                <Row justify="center" style={{ fontSize: 20 }}>
                  <Button type="text" size="large" icon={<MinusCircleOutlined />} onClick={() => sendMatchAction("P2_UNSCORED")} />
                  <Button type="text" size="large" icon={<PlusCircleOutlined />} onClick={() => sendMatchAction("P2_SCORED")} />
                </Row>
              </Card>
            </Col>
          </Row>
          <Row justify="center" style={{ marginTop: 40 }}>
            <Col span={8}>
              <Button
                style={{ width: "100%", height: "50px", fontSize: 20 }}
                type="primary"
                onClick={() => sendMatchAction("GAME_FINISHED")}
              >
                {t("finish")}
              </Button>
            </Col>
          </Row>
          <Row justify="center" style={{ marginTop: 10 }}>
            <Col span={8}>
              <Button
                style={{ width: "100%", height: "50px", fontSize: 20 }}
                danger
                onClick={() => sendMatchAction("GAME_CANCELED")}
              >
                {t("cancel")}
              </Button>
            </Col>
          </Row>
        </>
      )}
    </>
  );
};
export default CurrentGame;
