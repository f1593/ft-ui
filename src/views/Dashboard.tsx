import { Table } from "antd";
import Title from "antd/lib/typography/Title";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import { Player, Stats } from "../@types";
import PlayersContext from "../context/PlayersContext";

interface PlayerState extends Stats {
  key: string;
  name: string;
  ratio: number;
  goalDifference: number;
}

const calculatePlayersStats: (players: Player[]) => PlayerState[] = (players) => players
  .map((p) => ({
    name: p.name,
    key: p.uuid,
    ratio: Math.round((p.stats.wins / (p.stats.gamePlayed === 0 ? 1 : p.stats.gamePlayed)) * 100) / 100,
    goalDifference: p.stats.goalFor - p.stats.goalAgainst,
    ...p.stats,
  }))
  .sort((a, b) => b.ratio - a.ratio)
  .sort((a, b) => b.goalDifference - b.goalDifference);

const Dashboard = () => {
  const { t } = useTranslation();
  const { players } = React.useContext(PlayersContext);
  const [playersStats, setPlayersStats] = useState<PlayerState[]>([]);

  useEffect(() => setPlayersStats(calculatePlayersStats(players)), [players]);

  const columns = [{
    title: t("stats.name"),
    dataIndex: "name",
    key: "name",
    render: (text, record) => (<Link to={`/teams-players/${record.key}`}>{text}</Link>),
  }, {
    title: t("stats.gamesPlayed"),
    dataIndex: "gamePlayed",
    key: "gamePlayed",
  }, {
    title: t("stats.wins"),
    dataIndex: "wins",
    key: "wins",
  }, {
    title: t("stats.looses"),
    dataIndex: "looses",
    key: "looses",
  }, {
    title: t("stats.ratio"),
    dataIndex: "ratio",
    key: "ratio",
  }, {
    title: t("stats.goalFor"),
    dataIndex: "goalFor",
    key: "goalFor",
  }, {
    title: t("stats.goalAgainst"),
    dataIndex: "goalAgainst",
    key: "goalAgainst",
  }, {
    title: t("stats.goalDifference"),
    dataIndex: "goalDifference",
    key: "goalDifference",
  }];

  return (
    <>
      <Title>{t("dashboard")}</Title>
      <Table dataSource={playersStats} columns={columns} />
    </>
  );
};

export default Dashboard;
