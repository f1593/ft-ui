import {
  Card, message, Spin, Steps,
} from "antd";
import Title from "antd/lib/typography/Title";
import axios from "axios";
import moment from "moment";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

import { Game } from "../../@types";
import PlayersContext from "../../context/PlayersContext";
import config from "../../services/config";

import ParametersStep, { NewGameParams } from "./components/ParametersStep";
import PlayersStep, { SelectedPlayers } from "./components/PlayersStep";
import ScoreStep, { Scores } from "./components/ScoreStep";

const { Step } = Steps;

const NewGame = () => {
  const { t } = useTranslation();

  const baseSteps = [{
    title: t("newGameWizard.params"),
  }, {
    title: t("newGameWizard.players"),
  }];
  const scoreStep = {
    title: t("newGameWizard.scores"),
  };

  const [current, setCurrent] = useState(0);
  const [params, setParams] = useState<NewGameParams>({});
  const [currentPlayers, setCurrentPlayers] = useState<SelectedPlayers>({ player1: "", player2: "" });
  const [steps, setSteps] = useState<{title: string}[]>(baseSteps);
  const [loading, setLoading] = useState(false);
  const { refreshPlayers } = useContext(PlayersContext);
  const history = useHistory();

  const nextParams = (p: NewGameParams) => {
    setParams(p);
    if (p.type === "FINISHED_GAME") {
      setSteps([...baseSteps, scoreStep]);
    }
    if (p.type === "NEW_GAME") {
      setSteps(baseSteps);
    }
    setCurrent(current + 1);
  };

  const nextPlayers = (selectedPlayers: SelectedPlayers) => {
    setCurrentPlayers(selectedPlayers);
    if (params.type === "FINISHED_GAME") {
      setCurrent(current + 1);
    } else {
      setLoading(true);
      const game = {
        date: moment().format("YYYY-MM-DD HH:mm:ss"),
        ...selectedPlayers,
        score1: 0,
        score2: 0,
        status: "IN_PROGRESS",
      };
      axios.post<Game>(`${config.apiBaseHost}/game`, game)
        .then(async (res) => {
          setLoading(false);
          message.success(t("message.gameStarted"));
          localStorage.setItem("current-game", res.data.uuid);
          history.push("/current-game");
        })
        .catch(() => message.error(t("message.error")));
    }
  };

  const nextScores = (scores: Scores) => {
    setLoading(true);
    const game = {
      date: moment().format("YYYY-MM-DD HH:mm:ss"),
      ...currentPlayers,
      ...scores,
      status: "FINISHED",
    };
    axios.post(`${config.apiBaseHost}/game`, game)
      .then(async () => {
        setLoading(false);
        message.success(t("message.gameSaved"));
        await refreshPlayers();
        history.push("/");
      })
      .catch(() => message.error(t("message.error")));
  };

  return (
    <>
      <Title>{t("createNewGame")}</Title>
      <Card>
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">
          {current === 0 && <ParametersStep parameters={params} next={nextParams} />}
          {current === 1 && (
            <Spin spinning={loading}>
              <PlayersStep
                composition={params.composition}
                currentPlayers={currentPlayers}
                next={nextPlayers}
                previous={() => setCurrent(0)}
              />
            </Spin>
          )}
          {current === 2 && (
            <Spin spinning={loading}>
              <ScoreStep currentPlayers={currentPlayers} previous={() => setCurrent(1)} next={nextScores} />
            </Spin>
          )}
        </div>
      </Card>
    </>
  );
};

export default NewGame;
