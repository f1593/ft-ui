import {
  Row, Col, Button, Card, Select,
} from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import { Player, PlayerType } from "../../../@types";
import PlayersContext from "../../../context/PlayersContext";

import AddPlayer from "./AddPlayer";

interface Props {
  composition: PlayerType,
  currentPlayers: SelectedPlayers,
  next: (s: SelectedPlayers) => void,
  previous: () => void,
}

export interface SelectedPlayers {
  player1: string;
  player2: string
}

const PlayersStep: React.FC<Props> = ({
  composition, currentPlayers, next, previous,
}) => {
  const { t } = useTranslation();
  const { players } = useContext(PlayersContext);
  const [selectablePlayers, setSelectablePlayers] = useState<Player[]>([]);
  const [selectedPlayers, setSeletedPlayers] = useState<SelectedPlayers>(currentPlayers);

  useEffect(() => setSelectablePlayers(players.filter((p) => p.type === composition)), [players, composition]);

  return (
    <>
      <Row justify="center">
        <Col span={8}>
          <Card>
            <Row justify="center">
              {composition === "SOLO" ? t("stats.player1") : t("stats.team1")}
            </Row>
            <Row justify="center">
              <Select
                style={{ width: "100%", marginTop: 5 }}
                onChange={(val) => setSeletedPlayers({ ...selectedPlayers, player1: val.toString() })}
                value={selectedPlayers.player1}
              >
                {selectablePlayers.map((plyrs) => (
                  <Select.Option
                    key={`p1-${plyrs.uuid}`}
                    disabled={plyrs.uuid === selectedPlayers.player2}
                    value={plyrs.uuid}
                  >
                    {plyrs.name}
                  </Select.Option>
                ))}
              </Select>
            </Row>
          </Card>
        </Col>
        <Col offset={2} span={8}>
          <Card>
            <Row justify="center">
              {composition === "SOLO" ? t("stats.player2") : t("stats.team2")}
            </Row>
            <Row justify="center">
              <Select
                style={{ width: "100%", marginTop: 5 }}
                onChange={(val) => setSeletedPlayers({ ...selectedPlayers, player2: val.toString() })}
                value={selectedPlayers.player2}
              >
                {selectablePlayers.map((plyrs) => (
                  <Select.Option
                    key={`p2-${plyrs.uuid}`}
                    disabled={plyrs.uuid === selectedPlayers.player1}
                    value={plyrs.uuid}
                  >
                    {plyrs.name}
                  </Select.Option>
                ))}
              </Select>
            </Row>
          </Card>
        </Col>
      </Row>
      <AddPlayer type={composition} />
      <Row justify="center" style={{ marginTop: 10 }}>
        <Col span={2} style={{ margin: 10 }}>
          <Button onClick={previous}>
            {t("previous")}
          </Button>
        </Col>
        <Col span={2} style={{ margin: 10 }}>
          <Button
            type="primary"
            onClick={() => next(selectedPlayers)}
            disabled={selectedPlayers.player1 === "" || selectedPlayers.player2 === ""}
          >
            {t("next")}
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default PlayersStep;
