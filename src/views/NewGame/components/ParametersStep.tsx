import {
  CheckCircleOutlined, PlusCircleOutlined, TeamOutlined, UserOutlined,
} from "@ant-design/icons";
import {
  Button, Card, Col, Row,
} from "antd";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import { GameType, PlayerType } from "../../../@types";

export interface NewGameParams {
  type?: GameType,
  composition?: PlayerType
}

interface Props {
  parameters?: NewGameParams;
  next: (params: NewGameParams) => void;
}

const ParametersStep: React.FC<Props> = ({ parameters, next }) => {
  const { t } = useTranslation();
  const [params, setParams] = useState<NewGameParams>({});

  const selectType = (type: GameType) => () => setParams({ ...params, type });
  const selectCompo = (composition: PlayerType) => () => setParams({ ...params, composition });

  useEffect(() => setParams(parameters), [parameters]);

  return (
    <>
      <Row justify="center">
        <Col span={4}>
          <Card hoverable className={params.type === "NEW_GAME" ? "card-selected" : ""} onClick={selectType("NEW_GAME")}>
            <Row justify="center">
              <PlusCircleOutlined style={{ fontSize: 35, marginBottom: 5 }} />
            </Row>
            <Row justify="center">
              <span style={{ fontSize: 20 }}>{t("newGame")}</span>
            </Row>
          </Card>
        </Col>
        <Col offset={2} span={4}>
          <Card hoverable className={params.type === "FINISHED_GAME" ? "card-selected" : ""} onClick={selectType("FINISHED_GAME")}>
            <Row justify="center">
              <CheckCircleOutlined style={{ fontSize: 35, marginBottom: 5 }} />
            </Row>
            <Row justify="center">
              <span style={{ fontSize: 20 }}>{t("finishedGame")}</span>
            </Row>
          </Card>
        </Col>
      </Row>

      <Row justify="center" style={{ marginTop: 10 }}>
        <Col span={4}>
          <Card hoverable className={params.composition === "SOLO" ? "card-selected" : ""} onClick={selectCompo("SOLO")}>
            <Row justify="center">
              <UserOutlined style={{ fontSize: 35, marginBottom: 5 }} />
            </Row>
            <Row justify="center">
              <span style={{ fontSize: 20 }}>1 VS 1</span>
            </Row>
          </Card>
        </Col>
        <Col offset={2} span={4}>
          <Card hoverable className={params.composition === "TEAM" ? "card-selected" : ""} onClick={selectCompo("TEAM")}>
            <Row justify="center">
              <TeamOutlined style={{ fontSize: 35, marginBottom: 5 }} />
            </Row>
            <Row justify="center">
              <span style={{ fontSize: 20 }}>2 VS 2</span>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row justify="center" style={{ marginTop: 10 }}>
        <Col span={1} style={{ margin: 10 }}>
          <Button type="primary" onClick={() => next(params)} disabled={!params.type || !params.composition}>
            {t("next")}
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default ParametersStep;
