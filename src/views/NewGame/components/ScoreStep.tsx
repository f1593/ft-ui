import {
  Button, Card, Col, Input, Row,
} from "antd";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";

import PlayersContext from "../../../context/PlayersContext";

import { SelectedPlayers } from "./PlayersStep";

interface Props {
  currentPlayers: SelectedPlayers
  previous: () => void,
  next: (scores: Scores) => void,
}

export interface Scores {
  score1: number;
  score2: number;
}

const ScoreStep: React.FC<Props> = ({ currentPlayers, previous, next }) => {
  const { t } = useTranslation();
  const { players } = useContext(PlayersContext);
  const [scores, setScores] = useState<Scores>({ score1: 0, score2: 0 });

  const findPlayerName = (uuid) => players.find((p) => p.uuid === uuid).name;

  return (
    <>
      <Row justify="center">
        <Col span={8}>
          <Card>
            <Row justify="center" style={{ fontSize: 20 }}>
              {findPlayerName(currentPlayers.player1)}
            </Row>
            <Row justify="center">
              <Input
                type="number"
                value={scores.score1}
                onChange={(e) => setScores({ ...scores, score1: parseInt(e.target.value, 10) })}
              />
            </Row>
          </Card>
        </Col>
        <Col offset={2} span={8}>
          <Card>
            <Row justify="center" style={{ fontSize: 20 }}>
              {findPlayerName(currentPlayers.player2)}
            </Row>
            <Row justify="center">
              <Input
                type="number"
                value={scores.score2}
                onChange={(e) => setScores({ ...scores, score2: parseInt(e.target.value, 10) })}
              />
            </Row>
          </Card>
        </Col>
      </Row>
      <Row justify="center" style={{ marginTop: 10 }}>
        <Col span={2} style={{ margin: 10 }}>
          <Button onClick={previous}>
            {t("previous")}
          </Button>
        </Col>
        <Col span={2} style={{ margin: 10 }}>
          <Button
            type="primary"
            disabled={scores.score1 === 0 && scores.score2 === 0}
            onClick={() => next(scores)}
          >
            {t("next")}
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default ScoreStep;
