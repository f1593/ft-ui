import {
  Row, Col, Input, Button,
} from "antd";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";

import { PlayerType } from "../../../@types";
import PlayersContext from "../../../context/PlayersContext";

interface Props {
  type: PlayerType
}

const AddPlayer: React.FC<Props> = ({ type }) => {
  const { t } = useTranslation();
  const { addPlayer } = useContext(PlayersContext);
  const [name, setName] = useState<string>("");

  const onSave = () => {
    if (name.length === 0) return;
    addPlayer(name, type).then(() => setName(""));
  };

  return (
    <Row justify="center" style={{ marginTop: 10 }}>
      <Col span={3} style={{ margin: 10 }}>
        {type === "SOLO" ? t("newGameWizard.addPlayer") : t("newGameWizard.addTeam")}
      </Col>
      <Col span={6} style={{ margin: 10 }}>
        <Input
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder={type === "SOLO" ? t("newGameWizard.addPlayer") : t("newGameWizard.addTeam")}
        />
      </Col>
      <Col span={3} style={{ margin: 10 }}>
        <Button type="primary" onClick={onSave}>{t("newGameWizard.save")}</Button>
      </Col>
    </Row>
  );
};

export default AddPlayer;
