declare global {
  interface Window {
    config: Config
  }
}

export interface Config {
  apiBaseHost: string
}

const { config } = window;

export default config;
