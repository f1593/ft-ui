export type PlayerType = "SOLO" | "TEAM";
export type GameType = "NEW_GAME" | "FINISHED_GAME";

export interface Player {
  uuid: string;
  name: string;
  stats: Stats;
  type: PlayerType;
}

export interface Stats {
  gamePlayed: number;
  wins: number;
  looses: number;
  goalFor: number;
  goalAgainst: number;
}

export interface Game {
  uuid: string;
  date: string;
  player1: string;
  player2: string;
  score1: number;
  score2: number;
  status: "IN_PROGRESS" | "FINISHED" | "CANCELED"
}

export enum GameAction {
  P1_SCORED,
  P2_SCORED,
  P1_UNSCORED,
  P2_UNSCORED,
  GAME_CANCELED,
  GAME_FINISED
}
