# Description 📝
Your friends are table football addicts. Unfortunately, they are also extremely bad at keeping score, which results in endless fights about who is the table football champion. To solve the situation once and for all, you decide to write a simple program that keeps track of the score during the game, stores the final result after the game ends, and displays some statistics.


# How to run locally 🚀
This app is developped with React. You need to have node and npm installed in your computer. Execute the following steps for lunch the app locally

- npm install 
- npm run start

The server is running on the port 3000

# Demo environment 👀
This app is deployed on Firebase Hosting as a demo environment. You can access it with this url: https://football-table-333114.web.app/