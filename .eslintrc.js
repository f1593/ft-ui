module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "plugin:react/recommended",
    "airbnb",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: [
    "react",
    "@typescript-eslint",
  ],
  rules: {
    "no-use-before-define": "off",
    "react/prop-types": "off",
    "react/require-default-props": "off",
    "react/jsx-filename-extension": "off",
    "react/no-array-index-key": "off",
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    quotes: ["error", "double"],
    "max-len": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      "warn",
    ],
    "import/order": [
      "error",
      {
        "newlines-between": "always",
        groups: [
          "builtin",
          "external",
          "internal",
          "parent",
          [
            "sibling",
            "index",
          ],
        ],
      },
    ],
  },
};
